#ifndef ROBOT_HPP
#define ROBOT_HPP

#include <string>
#include "../../../system/joueur/Joueur.hpp"

class Robot : public Joueur
{
    
private:
    /* data */
public:
    Robot(string n);
    virtual ~Robot();
};

Robot::Robot(string n) : Joueur(n){}
Robot::~Robot(){}

#endif