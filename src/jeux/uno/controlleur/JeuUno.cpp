#include "JeuUno.hpp"
#define CONFIG_ERROR "Impossible de commencer le jeu, il manque une configuration"

JeuUno::JeuUno(int nb_c, int nb_j) : Jeu(), nb_carte(nb_c), nb_joueur(nb_j) {}

JeuUno::~JeuUno() {}

bool JeuUno::peutLancer() { return (joueurs.size() == nb_joueur && !this->paquet->isEmpty()); }

void JeuUno ::addJoueur(Joueur joueur)
{
    Joueur *j = new Joueur(joueur);
    joueurs.add(j);
    plateau.addJoueur(j);
}

void JeuUno::lancer()
{

    if (this->peutLancer())
    {

        this->partageCarte();

        this->plateau.setPioche(this->paquet);

        this->retoureUneCarte();

        //int next = 0;
        //bool joueurADejaPiocher = false;
        //Carte *cte = nullptr;

        while (!estTermine())
        {

            /*
            next = actionDuUno(next);
            do
            {
                cte = iu::askCarte((joueurADejaPiocher) ? string("Entrez a nouveau votre carte") : string("Entrez votre carte"), this->ordre(next)->getCartesJoueur()->toVector());
                if (cte == nullptr)
                {
                    if (this->nePeutPasJouer(this->ordre(next)) && !joueurADejaPiocher)
                    {
                        this->ordre(next)->prends(plateau.piocher());
                        joueurADejaPiocher = true;
                    }
                }
            } while (!this->estValide(cte) && joueurADejaPiocher);
            joueurADejaPiocher = false;
            next = (++next % (int)joueurs.size());

            */
        }
    }

    else
    {
        iu::annonce(CONFIG_ERROR);
    }
}

bool JeuUno ::estTermine()
{
    for (Joueur *j : this->joueurs.toVector())
    {
        if (j->getCartesJoueur()->size() == 0)
            return true;
    }
    return false;
}

int JeuUno::actionDuUno(int tourJoueur)
{
    Joueur *joueur = this->ordre(tourJoueur);
    Pile *p = this->plateau.plateau_pile_from(0);
    Carte *cte = p->get(0);
    CarteAction *cteAction = cast(cte);
    if (cteAction == nullptr)
    {
        iu::annonce("pas action");
        return tourJoueur;
    }

    else
    {

        iu::annonce(cteAction->print());
        switch (cteAction->getAction())
        {
        case PlusDeux:
        {
            joueur->prends(plateau.piocher());
            joueur->prends(plateau.piocher());
            return tourJoueur++;
        }
        break;

        case plusQuatre:
        {
            for (int i{0}; i < 4; i++)
            {
                joueur->prends(plateau.piocher());
            }
            vector<string> reponse{"oui", "nom"};
            int choix = iu::ask("Voulez-vous choisir une autre carte ? ", reponse);
            if (choix == 1)
            {
                if (joueur->isRobot())
                {
                    plateau.placer(joueur->joue(), 0);
                }
                else
                {
                    plateau.placer(iu::askCarte(string("Entrez votre carte"), joueur->getCartesJoueur()->toVector()), 0);
                }
            }
            return tourJoueur++;
        }
        break;

        case PasseTonTour:
        {
            return tourJoueur++;
        }
        break;

        case Joker:
        {
            vector<string> reponse{"oui", "nom"};
            int choix = iu::ask("Voulez-vous choisir une autre carte ? ", reponse);
            if (choix == 1)
            {
                if (joueur->isRobot())
                {
                    plateau.placer(joueur->joue(), 0);
                }
                else
                {
                    plateau.placer(iu::askCarte(string("Entrez votre carte"), joueur->getCartesJoueur()->toVector()), 0);
                }
            }
            return tourJoueur++;
        }

        break;

        default:
        {
            return tourJoueur;
        }
        break;
        }
    }
}

bool JeuUno::estValide(Carte *c)
{
    //return (*(plateau.voir(0)) == *c);
    return false;
}

void JeuUno::retoureUneCarte() { plateau.placer(plateau.piocher()); }

bool JeuUno::nePeutPasJouer(Joueur *joueur)
{
    return joueur->getCartesJoueur()->filter(&estJokerOuPlusQuatre).isEmpty();
}

void JeuUno::partageCarte()
{
    if (!peutLancer())
        return;
    for (int i = 0; i < this->nb_carte; i++)
        for (int j = 0; j < this->joueurs.size(); j++)
            this->joueurs(j)->prends(this->paquet->ramdomGetAndDelete());
}

void JeuUno::score() {}

bool estJokerOuPlusQuatre(Carte *c)
{
    CarteAction *cteA = cast(c);

    if (cteA == nullptr)
        return false;

    switch (cteA->getAction())
    {
    case Joker:
        return true;
        break;
    case plusQuatre:
        return true;
        break;
    default:
        return false;
        break;
    }
}

CarteAction *cast(Carte *cte)
{
    try
    {
        CarteAction *cteAction = dynamic_cast<CarteAction *>(cte);
        return cteAction;
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
        return nullptr;
    }
}