#ifndef JEUUNO_HPP
#define JEUUNO_HPP

#include <typeinfo>
#include <istream>
#include "../../../system/facade/Jeu.hpp"
#include "../../../system/joueur/Joueur.hpp"
#include "../../../system/facade/IU.hpp"
#include "../../../system/template/List.hpp"
#include "../../../system/enum/ConstEnum.hpp"
#include "../../../system/carte/CarteAction.hpp"
#include "../../../system/carte/Carte.hpp"

#define SCORE_GAGANT 500

using namespace std;

class JeuUno : public Jeu
{

private:
    List<Joueur> ordre;
    const int nb_carte;
    const int nb_joueur;
    bool direction = true;

    JeuUno() = delete;
    void partageCarte();
    void retoureUneCarte();
    bool estValide(Carte *);
    bool estTermine();
    bool peutLancer();
    int actionDuUno(int tourJoueur);
    bool nePeutPasJouer(Joueur *joueur);
    Joueur *randomJoeur();

public:
    JeuUno(int nb_c, int nb_j);
    virtual ~JeuUno();
    virtual void lancer();
    virtual void addJoueur(Joueur j);
    virtual void score();
};

bool estJokerOuPlusQuatre(Carte *);

CarteAction *cast(Carte *cte);

#endif