#ifndef TAROT_HPP
#define TAROT_HPP

#include "../../system/facade/Jeu.hpp"

class JeuTarot: public Jeu
{
protected:
  int scores[];
public:
  JeuTarot();
  virtual ~JeuTarot();
  virtual void init();
  virtual void lancer();
  virtual void score();

  void donne();
  bool fin();
  void tour();
};

#endif