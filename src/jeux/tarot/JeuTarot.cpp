#include <cstdlib> /* rand() */
#include "JeuTarot.hpp"
#include "../../system/facade/IU.hpp"
#include "../../system/builder/TarotBuilder.hpp"

using namespace std;

#define NBJ 4
#define NBJ_S "4"

#define BIENVENUE "Jeu du Tarot : " NBJ_S " joueurs"
#define ASK_Joueur "Entrez un nom de joueur"
#define ENCHERES_INI "Enchères pour le chien"
#define ENCHERE_PARI \
  "Si vous ne voulez pas enchérir entrez \"passer\", " \
  "sinon indiquez l'enchère que vous proposez"

JeuTarot::JeuTarot() :Jeu() {}

void JeuTarot::init(){
  iu::annonce(BIENVENUE);
  for(int i=0; i<NBJ;i++){
    Joueur *j = new Joueur(iu::askString(ASK_Joueur));
    joueurs.add(j);
    plateau.addJoueur(j,1);
  }
  paquet = TarotBuilder().build();
}

void JeuTarot::lancer(){
  donne();
  int joueurSuivt = 0;
  while(true){
    break;
  }
}

void JeuTarot::score(){}

void JeuTarot::donne(){
  iu::annonce(ENCHERES_INI);
  //== enchères ==
  int donneur = rand()%(NBJ-1); // on part d'où?
  Joueur *preneur = nullptr;
  int choix = -1;// 0=petite 1=garde 2=gardeS 3=gardeC
  while(preneur==nullptr){
    //== distribution ==
    int max = paquet.size();
    for(int j=0;j<max-6;j++)
      joueurs[j%NBJ]->prends(paquet.getAndDeleteRandom());
    for(int i=0;i<6;i++){
      Carte *c = paquet.getAndDeleteRandom();
      if(c->estDeFace()) c->retourner();
      plateau.placer(c,nullptr);
    }

    iu::printCommun(&plateau);

    int opt_size=4;
    string opt[]={"petite", "garde", "garde sans", "garde contre"};
    vector<string> donne_opt;
    int opts = 5, decalage=0;
    for(int i=0;i<joueurs.size();i++){
      int ii = (donneur+i)%joueurs.size();// curseur après rotation aléatoire
      donne_opt.push_back("passer");
      for(int j=decalage;j<opt_size;j++)
        donne_opt.push_back(opt[j]);

      iu::printJoueur(joueurs[ii]);
      int nouvchoix = iu::ask(ENCHERE_PARI,donne_opt);
      if(nouvchoix>0){
        choix = nouvchoix-1 + decalage;
        preneur = joueurs[ii];
      }
      donne_opt.erase(donne_opt.begin(),donne_opt.end());
    }
    if(preneur==nullptr){
      // TODO paquet.add(plateau.getAndRemoveAllCartes())
      // TODO paquet.add(joueurs.getAndRemoveAllCartes())
      iu::annonce("Récupération des cartes non implémenté !");
    }
  }
  if(choix<0) exit(EXIT_FAILURE);
  else if(choix==0){// petite
  }else if(choix==1){// garde
  }else if(choix==2){// garde sans
  }else if(choix==3){// garde contre
  }else exit(EXIT_FAILURE);
  iu::annonce("Tête de code");

}
