#include "JeuBataille.hpp"

#define CONFIG_ERROR "Impossible de commencer le jeu, il manque une configuration"
#define START "STARTING ... "
#define BATAILLE "BATAILLE en cours ... "
#define PARTAGE "Partage en cours ... "
#define PARTIE "Partie en cours ... "
#define FINPARTIE "Fin de la partie ..."
#define SCORE "LE GAGNANT DE CETTE BATAILLE EST LE JOUEUR : "
#define ASK_CARTE "Entrez votre carte : "

JeuBataille ::JeuBataille() {}

JeuBataille ::~JeuBataille() {}

void JeuBataille ::addJoueur(Joueur joueur)
{
    Joueur *j = new Joueur(joueur);
    joueurs.add(j);
    plateau.addJoueur(j);
}

void JeuBataille ::score()
{
    if (nePeutJoueur(joueurs(0)))
    {
        iu::annonce(SCORE + joueurs(0)->getNom());
    }else
    {
         iu::annonce(SCORE + joueurs(1)->getNom());
    }
    
}

bool JeuBataille ::nePeutJoueur(Joueur *j)
{
    return j->getCartesJoueur()->size() == 0;
}

bool JeuBataille ::estTermine() { return this->end; }
void JeuBataille::partageCarte()
{
    iu::annonce(PARTAGE);
    while (this->paquet->size() != 0)
    {
        try
        {
            joueurs(0)->prends(this->paquet->ramdomGetAndDelete());
            joueurs(1)->prends(this->paquet->ramdomGetAndDelete());
        }
        catch (const std::exception &e)
        {
            std::cerr << e.what() << '\n';
        }
    }
}

bool JeuBataille ::peutLancer() { return (joueurs.size() == NB_Joueur && !this->paquet->isEmpty()); }

Joueur *JeuBataille::manche()
{
    iu::annonce(BATAILLE);
    if (nePeutJoueur(joueurs(0)) || nePeutJoueur(joueurs(1)))
    {
        end = true;
        return nullptr;
    }

    Carte *choix1 = iu::askCarte(ASK_CARTE + joueurs(0)->getNom(), joueurs(0)->getCartesJoueur()->toVector());
    Carte *choix2 = iu::askCarte(ASK_CARTE + joueurs(1)->getNom(), joueurs(1)->getCartesJoueur()->toVector());
    joueurs(0)->getCartesJoueur()->moveToList(choix1);
    joueurs(1)->getCartesJoueur()->moveToList(choix2);
    plateau.placer(choix1);
    plateau.placer(choix2);

    if (choix1->getPoint() == choix2->getPoint())
    {
        return manche();
    }
    else
    {
        return (choix1->getPoint() > choix2->getPoint()) ? joueurs(0) : joueurs(1);
    }
}

void JeuBataille ::lancer()
{

    iu::annonce(START);
    if (this->peutLancer())
    {
        partageCarte();
        iu::annonce(PARTIE);
        Joueur *gagnant = nullptr;
        while (!estTermine())
        {
            gagnant = this->manche();
            iu::annonce("Gagnant de la manche  : " + gagnant->print());
            if (gagnant == nullptr)
                break;
            plateau.placer(plateau.getPileAndMove(), 0, gagnant);

            if (joueurs(0)->getCartesJoueur()->size() == 0)
            {
                iu::annonce("la bataille continue avec " + joueurs(0)->print());
                joueurs(0)->prends(plateau.getPileAndMove(0, joueurs(0)));
            }

            if (joueurs(1)->getCartesJoueur()->size() == 0)
            {
                iu::annonce("la bataille continue avec " + joueurs(1)->print());
                joueurs(1)->prends(plateau.getPileAndMove(0, joueurs(1)));
            }
        }
        iu::annonce(FINPARTIE);
        this->score();
    }
    else
    {
        iu::annonce(CONFIG_ERROR);
    }
}
