#ifndef JEUBATAILLE_HPP
#define JEUBATAILLE_HPP
#define NB_Joueur 2

#include <vector>
#include "../../system/facade/Jeu.hpp"
#include "../../system/facade/Plateau.hpp"
#include "../../system/joueur/Joueur.hpp"

class JeuBataille : public Jeu
{
private:
  bool end = false;

public:
  JeuBataille();
  virtual ~JeuBataille();
  void lancer();
  void addJoueur(Joueur joueur);
  void score();
  bool peutLancer();
  bool estTermine();
  void partageCarte();
  bool nePeutJoueur(Joueur *j);
  Joueur *manche();
};

#endif