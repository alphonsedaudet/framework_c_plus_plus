#include <iostream>
#include "system/facade/Jeu.hpp"
#include "system/builder/AbstractBuilder.hpp"
#include "system/builder/UnoBuilder.hpp"
#include "system/builder/BatailleBuilder.hpp"
#include "jeux/uno/controlleur/JeuUno.hpp"
#include "jeux/bataille/JeuBataille.hpp"

using namespace std;

int main(int argc, char const *argv[])
{

  /**
   * exemple d'utilisation du framework avec le jeu UNO
  Jeu *uno = new JeuUno(7, 2);
  uno->setPaquet(UnoBuilder(TaillePioche::CentEtHuit).build());
  uno->addJoueur(new Joueur(string("ubuntu")));
  uno->addJoueur(new Joueur(string("centos")));
  uno->lancer();
  uno->score();
  delete uno;
  */

  /*
  const AbstractBuilder &builder = UnoBuilder(TaillePioche::CentEtHuit);
  const int JOUEUR = 2;
  const int NB_CARTES = 7;
  Jeu *uno = new JeuUno(NB_CARTES, JOUEUR);
  uno->setPaquet(builder.build());
  uno->addJoueur(Joueur("ubuntu"));
  uno->addJoueur(Joueur("centos"));
  uno->lancer();
  delete uno;
  */

  const AbstractBuilder &builder = BatailleBuilder(TaillePioche::TrenteDeux);
  Jeu *bataille = new JeuBataille();
  bataille->setPaquet(builder.build());
  bataille->addJoueur(Joueur("ubuntu"));
  bataille->addJoueur(Joueur("centos"));
  bataille->lancer();
  delete bataille;

  return 0;
}
