#ifndef JOUEUR_HPP
#define JOUEUR_HPP

#include <string>
#include "../carte/Carte.hpp"
#include "../template/List.hpp"

using namespace std;

class Joueur
{
private:
  Joueur() = delete;

protected:
  const string nom;
  List<Carte> *cartesJoueur;

public:
  Joueur(string n);
  Joueur(const Joueur &cpy) : nom(cpy.nom) { cartesJoueur = new List<Carte>(); };
  string getNom() const { return nom; }
  virtual ~Joueur();
  virtual List<Carte> *getCartesJoueur() const;
  virtual void prends(Carte *c);
  void prends(vector<Carte *> list);
  virtual bool isRobot() const;
  virtual Carte *joue();
  virtual string print() const;
  friend ostream &operator<<(ostream &os, const Joueur &j)
  {
    return os << j.print();
  }
};

#endif
