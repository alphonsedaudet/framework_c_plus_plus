
#include "Joueur.hpp"

Joueur::Joueur(string n) : nom(n) { cartesJoueur = new List<Carte>(); }

Joueur::~Joueur() { delete cartesJoueur; }

List<Carte> *Joueur::getCartesJoueur() const
{
    return this->cartesJoueur;
}

Carte *Joueur::joue() { return nullptr; }

void Joueur::prends(Carte *c)
{
    cartesJoueur->add(c);
}

void Joueur::prends(vector<Carte *> list)
{
    for (Carte *c : list)
        cartesJoueur->add(c);
}

bool Joueur::isRobot() const
{
    return false;
}

string Joueur::print() const
{
    return "Joueur " + nom;
}