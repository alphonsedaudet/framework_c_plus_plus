#ifndef JEU_HPP
#define JEU_HPP

#include "Plateau.hpp"
#include "IU.hpp"
#include "../joueur/Joueur.hpp"
#include "../carte/Carte.hpp"
#include "../template/List.hpp"

class Jeu
{

protected:
  Plateau plateau;
  List<Joueur> joueurs;
  List<Carte> *paquet;
  Jeu() : plateau(), joueurs(), paquet() {}

public:
  virtual ~Jeu() { delete paquet; };
  virtual void lancer() = 0;
  virtual void setPaquet(List<Carte> *p) { this->paquet = p; };
  virtual void addJoueur(Joueur joueur) = 0;
  virtual void score() = 0;
};

#endif