#ifndef PLATEAU_HPP
#define PLATEAU_HPP

#include <map>
#include <vector>
#include <exception>
#include "../carte/Carte.hpp"
#include "../template/List.hpp"
#include "../joueur/Joueur.hpp"

typedef List<Carte> Pile;
typedef List<Pile> Zone;

using namespace std;

class Plateau
{

private:
  Pile *pioche;
  Pile *defausse;
  Zone *zone;
  map<Joueur *, Zone *> joueurZone;

public:
  Plateau();
  virtual ~Plateau();

  void setPioche(Pile *p);

  void addJoueur(Joueur *joueur);

  Pile *getPioche() { return pioche; }

  /* renvoie la pile p de la zone du joueur */
  Pile *plateau_pile_from(const int p = 0, Joueur *z = nullptr) const;

  /* taille de la zone du joueur j; commune  si nullptr */
  int tailleZone(Joueur *j) const;

  /* taille d'une pile de la zone indiquée */
  int taillePile(Joueur *j, int pile);

  /* renvoie un pointeur Carte* retirée du haut de la pioche */
  Carte *piocher();

  void defausser(Carte *c); // ajoute la carte au haut du la défausse

  /* ajoute c à la pile-ème pile du joueur */
  void placer(Carte *c, int NPile = 0, Joueur *j = nullptr);

  void placer(vector<Carte *> cartes, int NPile = 0, Joueur *j = nullptr);

  vector<Carte *> getPileAndMove(int pile = 0, Joueur *j = nullptr);

  /* ajoute une pile à la zone du joueur j */
  //void addPile(Joueur *j);

  /* ajoute une zone associée à un joueur */
  void addPile(const int nbPile, Joueur *joueur);

  /* déplace la carte en gaut de la pile @pile1 à l'emplacement indiqué */
  //void bouger(Joueur *joueur1, int pile1, Joueur *joueur2, int pile2);

  // renvoie un pointeur sur la carte attendue
  //Carte *voir(int place, Joueur *zones = nullptr) const;

  // reprend le code de voir(Joueur*,int) et retire la carte de son emplacement
  //Carte *prendre(Joueur *joueur, int pile);

  // renvoie un pointeur sur la zone attendue
  List<Carte> *voirPile(int pile = 0, Joueur *j = nullptr) const;

  // retirer toutes les cartes du plateau et les renvoie
  //vector<Carte *> rassembler();
};

#endif