#include "IU.hpp"

// couleurs
#define Sstart "\033["
#define Send "m"
#define RESET ";0"
#define Sreset Sstart RESET Send
// style
#define BOLD ";1"
#define UL ";4"
#define ULL ";21"
#define OL ";53"
// font colors - darker
#define BLACK ";30"
#define RED ";31"
#define GREEN ";32"
#define YELLOW ";33"
#define BLUE ";34"
#define MAGENTA ";35"
#define CYAN ";36"
#define WHITE ";37"
// font colors - bright
#define BrBLACK ";90"
#define BrRED ";91"
#define BrGREEN ";92"
#define BrYELLOW ";93"
#define BrBLUE ";94"
#define BrMAGENTA ";95"
#define BrCYAN ";96"
#define BrWHITE ";97"
// background colors - darker
#define BgBLACK ";40"
#define BgRED ";41"
#define BgGREEN ";42"
#define BgYELLOW ";43"
#define BgBLUE ";44"
#define BgMAGENTA ";45"
#define BgCYAN ";46"
#define BgWHITE ";47"
// background colors - bright
#define BgBrBLACK ";100"
#define BgBrRED ";101"
#define BgBrGREEN ";102"
#define BgBrYELLOW ";103"
#define BgBrBLUE ";104"
#define BgBrMAGENTA ";105"
#define BgBrCYAN ";106"
#define BgBrWHITE ";107"
// iu colors & messages
#define TYPO_INTERN1 Sstart RESET BOLD CYAN Send
#define TYPO_INTERN2 Sstart RESET BOLD WHITE BgRED Send
#define TYPO_WARN Sstart RESET BOLD YELLOW Send
#define ERROR TYPO_INTERN2 " Problème interne : "
#define CARTE_INI "[" Sstart RESET BOLD BLACK BgBrWHITE Send
#define CARTE_CACHEE "-face cachée-"
#define CARTE_END Sreset "]"
#define PRINTT TYPO_INTERN1 "C'est au tour du joueur "
#define PRINTJ Sstart RESET BOLD BLACK BgWHITE Send
#define ASKPTR_AGAIN TYPO_INTERN2 " Veuillez entrer un nombre valide. " Sreset
#define ASK_NOA " réponses non fournies "
#define ASK_1A "Saisie automatique"
#define ASK_Q TYPO_INTERN1
#define ASK_A Sstart RESET BOLD WHITE BgBLACK Send
#define ASK_PROMPT Sstart RESET BOLD GREEN Send "$> " Sreset
#define ASK_AGAIN TYPO_INTERN2 " Veillez taper une entrée valide. " Sreset
#define ASKN_AGAIN TYPO_INTERN2 " Veillez entrer un nombre. " Sreset
#define ANN TYPO_INTERN1
#define LOG " " Sstart RESET BOLD MAGENTA BgBrYELLOW Send " log: "
// iu values
#define ASK_HOW_MANY 5

using namespace std;

//// chantier ========== [
/*
vector<string> drawCartes(vector<Carte> v){
  if(v.size()==0) return vector<string>(0);
  if(v.size()==1) return drawCarte(&v.back());
  Carte *c = &v.back();
  vector<string> drawingCarte = drawCarte(c);
  // TODO
  return drawingCarte;
}*/

/* Renvoie un nouveau vector<string> avec @front en premier plan et @c en
   arrière plan (décalage de 1 vers la gauche et (description.hauteur)+1 de
   haut. */
/*
vector<string> drawNewCarte(vector<string> front, Carte *c){
  if(front.size()==0 || c==nullptr) return vector<string>(0);
  vector<string> descr = drawDescription(c->draw());
  int height = front.size() + descr.size();
  int width = front.back().size() +1;
  for(auto i=descr.begin(); i!=descr.end(); i++)
    if(i.size()>width-2) width = i.size()+2;
  // TODO verify
  vector<string> drawing = vector<string>(height);
}*/

/* Renvoie la description découpée au niveau des '\n'. */
/*
vector<string> drawDescription(const string &descr){
  if(descr.length()==0) return vector<string>(0); // empty description
  int len = 1+count(descr.begin(), descr.end(), '\n'); // nombre de lignes
  vector<string> lines = vector<string>(len);
  int pos = 0;
  string::size_t end = 0;
  for(int i=0; i<len; i++){
    end = find("\n",pos);
    if(end==string::npos){
      lines.push_back(descr.substr(pos));
      break;
    }
    lines.push_back(descr.substr(pos, end-pos));
    pos = end+1;
  }
  return lines
}*/

/*
vector<string> drawCarte(Carte *c){
  if (c==nullptr) return vector<string>(0);
  string descr = c->draw();
  vector<string> lines = drawDescription(descr);
  int len = lines.size();
  int width = 0;
  for(auto i=lines.begin(); i!=lines.end(); i++){
    int a = i.length();
    if(a>width) width=a;
  }
  vector<string> drawing = vector<string>(0);
  string horBorder = COL_RESET;
  for(int i=0; i<len+2; i++)
    if(i==0||i==len+1) horBorder = horBorder + "+";
    else horBorder = horBorder + "-";
  drawing.push_back(hor);
  for(int i=0; i<len; i++){
    string d = "|" COL_START WHITE_BG COL_END;
    d = d + lines[i];
    for(int j=lines[i].length(); j<width; j++){
      d = d + " ";
    }
    d += COL_RESET "|";
    drawing.push_back(d);
  }
  drawing.push_back(horBorder);
  return drawing;
}
*/

/*
vector<string> stackCartes(vector<string> base, vector<string> carte){
  return vector<string>(0);
}*/

/*
vector<string> drawZone(vector<vector<Carte>> z){
  cout<<"drawnZone"<<endl;
  return vector<string>(0);
}*/
//// chantier ========== ]

string simpleDrawCartes(const vector<Carte *> *liste, bool respecterFace = true)
{
  if (liste == nullptr)
    return "";
  string r = "";
  for (auto i = liste->begin(); i != liste->end(); i++)
  {
    r += CARTE_INI;
    if (respecterFace && !(*i)->estDeFace())
      r += CARTE_CACHEE;
    else
      r += (*i)->print();
    r += CARTE_END;
  }
  return r;
}

void iu::printCommun(const Plateau *p)
{
  if (p == nullptr)
    return;
  cout << TYPO_INTERN1 "Zone commune : " Sreset << endl;
  for (int zs = 0; zs < p->tailleZone(nullptr); zs++)
  {
    Pile *pile = p->voirPile(zs, nullptr);
    if (pile == nullptr)
    {
      cout << ERROR "[Pile vide] " Sreset << endl
           << p->tailleZone(nullptr) << endl;
      return;
    }
    vector<Carte *> v = pile->toVector();
    string carte = simpleDrawCartes(&v, true);
    if (carte.size() == 0)
      cout << TYPO_WARN "[Zone vide]" Sreset << endl;
    else
      cout << carte << endl;
  }
}

void iu::printTour(const Plateau *p, const string joueur)
{
  printCommun(p);
  cout << PRINTT << joueur << Sreset << endl;
}

void iu::printJoueur(const Joueur *j)
{
  cout << PRINTJ << j->print() << Sreset << endl;
  vector<Carte *> v = j->getCartesJoueur()->toVector();
  cout << simpleDrawCartes(&v, false) << endl;
}

Carte *iu::askCarte(const string question, const vector<Carte *> cartes)
{

  cout << ASK_Q << question << Sreset << endl;
  if (cartes.size() <= 0)
  {
    cout << ERROR ASK_NOA Sreset << endl;
    return nullptr;
  }
  else if (cartes.size() == 1)
  {
    cout << ASK_1A << " " << ASK_A << cartes[0] << endl;
    return cartes[0];
  }
  int entree;
  int plafond = ASK_HOW_MANY;
  while (plafond > 0)
  {

    for (int i = 0; i < (int)cartes.size(); i++)
    {
      if ((cartes[i]->estDeFace()))
      {
        cout << "{ " ASK_A << i << " : " << cartes[i]->print() << Sreset " }";
      }
      else
      {
        cout << "{ " ASK_A << i << Sreset " }";
      }
    }
    cout << endl
         << ASK_PROMPT;
    cin >> entree;
    if (cin.fail())
    {
      cin.clear();
      cin.ignore();
      cout << ASKPTR_AGAIN << endl;
    }
    else
      return cartes[entree];
  }
  return nullptr;
}

int iu::ask(const string question, const string reponses[], int size)
{
  cout << ASK_Q << question << Sreset << endl;
  if (size <= 0)
  {
    cout << ERROR ASK_NOA Sreset << endl;
    return -1;
  }
  else if (size == 1)
  {
    cout << ASK_1A << " " << ASK_A << reponses[0] << endl;
    return 0;
  }
  string entree;
  int plafond = ASK_HOW_MANY;
  while (plafond > 0)
  {
    for (int i = 0; i < size; i++)
    {
      cout << "{ " ASK_A << reponses[i] << Sreset " }";
    }
    cout << endl
         << ASK_PROMPT;
    cin >> entree;
    for (int i = 0; i < size; i++)
    {
      if (entree.compare(reponses[i]) == 0)
        return i;
    }
    cout << ASK_AGAIN << endl;
  }
  return -1;
}

int iu::ask(const string question, const vector<string> reponses)
{
  cout << ASK_Q << question << Sreset << endl;
  if (reponses.size() <= 0)
  {
    cout << ERROR ASK_NOA Sreset << endl;
    return -1;
  }
  else if (reponses.size() == 1)
  {
    cout << ASK_1A << " " << ASK_A << reponses[0] << endl;
    return 0;
  }
  string entree;
  int plafond = ASK_HOW_MANY;
  while (plafond > 0)
  {
    for (auto i = reponses.begin(); i != reponses.end(); i++)
    {
      cout << "{ " ASK_A << *i << Sreset " }";
    }
    cout << endl
         << ASK_PROMPT;
    cin >> entree;
    for (int i = 0; i < (int)reponses.size(); i++)
    {
      if (entree.compare(reponses[i]) == 0)
        return i;
    }
    cout << ASK_AGAIN << endl;
  }
  return -1;
}

string iu::askString(const string question)
{
  cout << ASK_Q << question << Sreset << endl;
  string r;
  cout << ASK_PROMPT;
  cin >> r;
  return r;
}

int iu::askNumber(const string question)
{
  cout << ASK_Q << question << Sreset << endl;
  int entree;
  int plafond = ASK_HOW_MANY;
  while (plafond > 0)
  {
    cout << ASK_PROMPT;
    cin >> entree;
    if (cin.fail())
    {
      cin.clear();
      cin.ignore(999999, '\n');
      if (cin.eof())
      {
        cout << endl
             << TYPO_INTERN2 "Fin de lecture inattendue." Sreset << endl;
        return -1;
      }
      cout << ASKN_AGAIN << endl;
    }
    else
      return entree;
    plafond++;
  }
  return -1;
}

void iu::annonce(const string annonce)
{
  cout << ANN << annonce << Sreset << endl;
}

void log(const string msg, const string tag)
{
  cout << LOG << tag << " — " << msg << " " Sreset << endl;
}
