#ifndef IU_HPP
#define IU_HPP

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "Plateau.hpp"
#include "../template/List.hpp"
#include "../carte/Carte.hpp"
#include "../joueur/Joueur.hpp"
#include "../facade/Plateau.hpp"

namespace iu
{
  void printCommun(const Plateau *p);
  void printTour(const Plateau *p, const string joueur);
  void printJoueur(const Joueur *j);
  //void printText(const string &text); // -> annonce

  // renvoie le pointeur correspondant à la Carte de @cartes
  Carte *askCarte(const string question, const vector<Carte *> cartes);
  // renvoie l'indice de la réponse choisie
  int ask(const string question, const string reponses[], int size);
  int ask(const string question, const vector<string> reponses);
  // pose la question et renvoie la saisie
  string askString(const string question);
  // renvoie l'entier saisi
  int askNumber(const string question);
  // affiche une annonce
  void annonce(const string annonce);

  // affichage spécial pour les logs
  void log(const string msg, const string tag = "");
} // namespace iu

#endif