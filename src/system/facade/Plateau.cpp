#include "Plateau.hpp"

Plateau::Plateau() : joueurZone()
{
  zone = new Zone();
  zone->add(new Pile());
}
Plateau::~Plateau() {}

void Plateau::setPioche(Pile *p) { this->pioche = p; }

void Plateau::addJoueur(Joueur *joueur)
{
  joueurZone.insert({joueur, new Zone()});
  map<Joueur *, Zone *>::const_iterator pos = joueurZone.find(joueur);
  if (pos != joueurZone.end())
  {
    pos->second->add(new Pile());
  }
}

Pile *Plateau::plateau_pile_from(const int p, Joueur *j) const
{
  if (j == nullptr)
  {
    if ((int)zone->size() <= p)
    {
      return nullptr;
    }
    return zone->get(p);
  }
  map<Joueur *, Zone *>::const_iterator pos = joueurZone.find(j);

  //if (pos == joueurZone.end())
  //return nullptr;
  if ((int)pos->second->size() <= p)
    return nullptr;
  return pos->second->get(p);
}

int Plateau::tailleZone(Joueur *joueur) const
{
  if (joueur == nullptr)
    return zone->size();
  map<Joueur *, Zone *>::const_iterator pos = joueurZone.find(joueur);
  if (pos == joueurZone.end())
    return -1;
  return pos->second->size();
}

void Plateau::addPile(const int nbPile, Joueur *j)
{
  if (j == nullptr)
  {
    for (int i{0}; i < nbPile; i++)
      zone->add(new Pile());
    return;
  }
  map<Joueur *, Zone *>::const_iterator pos = joueurZone.find(j);
  if (pos == joueurZone.end())
    return;
  for (int i{0}; i < nbPile; i++)
  {
    pos->second->add(new Pile());
  }
}

Carte *Plateau::piocher()
{
  if (pioche->size() == 0)
    return nullptr; // Pioche vide
  return pioche->ramdomGetAndDelete();
}

void Plateau::defausser(Carte *c) { defausse->add(c); }

void Plateau::placer(Carte *c, int NPile, Joueur *joueur)
{
  //std::cout<<"placer…";
  if (c == nullptr)
    return;
  Pile *pile = plateau_pile_from(NPile, joueur);
  if (pile != nullptr)
  {
    pile->add(c);
  }
}

void Plateau::placer(vector<Carte *> cartes, int NPile, Joueur *joueur)
{
  for (Carte *c : cartes)
    this->placer(c, NPile, joueur);
}

vector<Carte *> Plateau::getPileAndMove(int pile, Joueur *joueur)
{
  vector<Carte *> result;
  Pile *p = plateau_pile_from(pile, joueur);
  if (p != nullptr)
  {
    result = p->toVector();
    p->clear();
  }
  return result;
}

List<Carte> *Plateau::voirPile(int pile, Joueur *joueur) const
{
  return plateau_pile_from(pile, joueur);
}

/*
void Plateau::bouger(Joueur *joueur1, const int pile1,
                     Joueur *joueur2, const int pile2)
{
  Pile *pileA = plateau_pile_from(joueur1, pile1);
  Pile *pileB = plateau_pile_from(joueur2, pile2);
  if (pileA == nullptr || pileB == nullptr)
    return;
  Carte *c = pileA->end();
  if (c == nullptr)
    return;
  pileB->add(pileA->getAndDelete(pileA->endIndex()));
}

*/

/*
Carte *Plateau::voir(int pile, Joueur *Joueur) const
{
  Pile *pile = plateau_pile_from(pile, place);
  if (pile == nullptr)
    return nullptr;
  return pile->end();
}
*/

/*
Carte *Plateau::prendre(Joueur *zone, int place)
{
  Pile *pile = plateau_pile_from(zone, place);
  if (pile == nullptr)
    return nullptr;
  Carte *c = pile->end();
  if (c == nullptr)
    return nullptr;
  pile->getAndDelete(pile->endIndex());
  return c;
}

*/

/*
vector<Carte *> Plateau::rassembler()
{
  vector<Carte *> r(0);
  for (auto z = zones.begin(); z != zones.end(); z++)
  {
    if (*z == nullptr)
      continue;
    for (auto p = (*z)->begin(); p != (*z)->end(); p++)
    {
      if (*p == nullptr)
        continue;
      for (int c = 0; c < (*p)->size(); c++)
        r.push_back((*p)->getAndDelete(c));
    }
  }
  return r;
}

*/
