#ifndef FABRIQUEJOUEUR_HPP
#define FABRIQUEJOUEUR_HPP

#include <iostream>
using namespace std;

class Joueur;

class FabriqueJoueur
{
    private:
        FabriqueJoueur() = delete;
        FabriqueJoueur(const FabriqueJoueur & cpy);
        virtual ~FabriqueJoueur();

    public:
        static Joueur* createJoueur(string n);
    
};


#endif