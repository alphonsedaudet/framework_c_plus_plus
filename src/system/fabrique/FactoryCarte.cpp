#include "FactoryCarte.hpp"

Carte *FactoryCarte::buildSimpleCarte(
    int p, Enseigne e, Valeur v, Couleur c, bool deFace)
{
    return new CarteSimple(p, e, v, c, deFace);
}

Carte *FactoryCarte::buildValueeCarte(int p, Valeur v, Couleur c, bool deFace)
{
    return new Valuee(p, v, c, deFace);
}

Carte *FactoryCarte::buildActionCarte(int p, Action a, Couleur c, bool deFace)
{
    return new CarteAction(p, c, a, deFace);
}
