#ifndef FACTORYCARTE_HPP
#define FACTORYCARTE_HPP

#include <iostream>
#include "../carte/Carte.hpp"
#include "../carte/CarteSimple.hpp"
#include "../carte/CarteAction.hpp"
#include "../enum/ConstEnum.hpp"

using namespace std;

class FactoryCarte
{
private:
  FactoryCarte() = delete;

public:
  static Carte *buildSimpleCarte(int p, Enseigne e, Valeur v, Couleur c = Couleur::None, bool deFace = false);
  static Carte *buildValueeCarte(int p, Valeur v, Couleur c = Couleur::None, bool deFace = false);
  static Carte *buildActionCarte(int p, Action a, Couleur c = Couleur::None, bool deFace = false);
};

#endif
