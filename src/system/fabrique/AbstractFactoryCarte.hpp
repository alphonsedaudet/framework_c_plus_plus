#ifndef AbstractFactoryCarte_HPP
#define AbstractFactoryCarte_HPP

#include <iostream>
#include "../carte/Carte.hpp"
#include "../enum/ConstEnum.hpp"

using namespace std;

class AbstracFactoryCarte
{
public:
  virtual Carte* makeSimple(int p, Enseigne e, Valeur v) = 0;
  virtual Carte* makeAtout(int p, Valeur v);
  virtual Carte* of(Carte &c) = 0;
};

#endif
