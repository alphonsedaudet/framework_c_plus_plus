#ifndef LIST_HPP
#define LIST_HPP

#include <exception>
#include <algorithm>
#include <random>
#include <vector>

//#include <cstdlib>
//#include <time.h>

using namespace std;

template <class T>

class List : public exception
{

private:
    mutable vector<T *> list;

    bool isPresent(T *t) const { return t != nullptr; }

    T &orElseThrow(T *t) const
    {
        if (!isPresent(t))
            throw string("pointeur null!");
        return *t;
    }

    int ramdomNumber(int n)
    {
        srand(time(NULL));
        return (n > 0) ? (rand() % n) : n;
    }

public:
    List<T>() {}

    List<T>(vector<T *> _list) : list(_list) {}

    //List<T>(const List<T> &cpy) = delete;

    virtual ~List<T>() { deleteAll(); }

    bool isEmpty() const { return this->list.empty(); }

    operator bool() const { return isEmpty(); }

    void add(T *t) { this->list.push_back(t); }

    void add(vector<T *> _list)
    {
        for (int i{0}; i < _list.size(); i++)
        {
            list.push_back(_list.at(i));
        }
    }

    T *ramdomGetAndDelete()
    {
        int i = ramdomNumber(list.size());
        T *p = list.at(i);
        moveToList(i);
        return p;
    }

    T *randomGet() { return list.at(ramdomNumber(list.size())); }

    vector<T *> toVector() const
    {
        return this->list;
    }

    T *get(int i) const
    {
        if (i >= 0 && i < (int)this->list.size())
            return this->list.at(i);
        return nullptr;
    }
    T *operator[](int i) const { return this->list(i); }
    T *operator()(int i) const { return get(i); }

    T *getAndDelete(int i)
    {
        T *copy = this->list[i];
        moveToList(copy);
        return copy;
    }

    bool moveToList(T *t)
    {
        for (auto i = list.begin(); i != list.end(); i++)
        {
            if (**i == *t)
            {
                list.erase(i);
                return true;
            }
        }
        return false;
    }

    void moveToList(unsigned int i) const
    {
        if (i < list.size())
        {
            list.erase(list.begin() + i);
        }
    }

    void deleteAll() const
    {
        for (int i = 0; i < (int)list.size(); i++)
            free(i);
    }

    void free(unsigned int i) const
    {
        if (list.at(i) != nullptr && i < list.size() && i >= 0)
        {
            delete list.at(i);
            moveToList(i);
        }
    }

    T *begin() { return list.at(beginIndex()); }
    T *end() { return list.at(endIndex()); }

    bool contains(T *t) const
    {
        for (T *e : this->list)
            if (e == t)
                return false;
        return false;
    }

    int size() const { return (int)this->list.size(); }

    int beginIndex() const { return 0; }
    int endIndex() const { return (this->list.size() - 1); }

    void clear()
    {
        list.clear();
    }

    List<T> filter(bool (*predicat)(T *t)) const
    {
        List<T> result;
        for (T *obj : this->list)
        {
            if ((*predicat)(obj))
                result.add(obj);
        }
        return result;
    }

    /**
     * mélange aléatoire des elements du vector list 
    */
    void shuffle() const
    { /*random_shuffle(list.begin(), list.end(), pointer_to_unary_function<T *, T *>(Rand)); */
    }
};

#endif