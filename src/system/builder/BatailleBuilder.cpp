#include "BatailleBuilder.hpp"

BatailleBuilder ::BatailleBuilder(const TaillePioche t) : T{t} {}

BatailleBuilder ::~BatailleBuilder() {}

List<Carte> *BatailleBuilder::build() const
{
    List<Carte> *paquet = new List<Carte>();

    for (int e = Pique; e <= Carreau; e++)
        for (int v = Valet; v <= As; v++)
            paquet->add(FactoryCarte::buildSimpleCarte(v, static_cast<Enseigne>(e), static_cast<Valeur>(v), Couleur::None, false));

    switch (this->T)
    {
    case TrenteDeux:
        for (int e = Pique; e <= Carreau; e++)
            for (int v = Sept; v <= Dix; v++)
                paquet->add(FactoryCarte::buildSimpleCarte(v, static_cast<Enseigne>(e), static_cast<Valeur>(v), Couleur::None, false));
        break;

    case CinquanteDeux:
        for (int e = Pique; e <= Carreau; e++)
            for (int v = Deux; v <= Dix; v++)
                paquet->add(FactoryCarte::buildSimpleCarte(v, static_cast<Enseigne>(e), static_cast<Valeur>(v)));
        break;
    default:
        break;
    }

    return paquet;
}