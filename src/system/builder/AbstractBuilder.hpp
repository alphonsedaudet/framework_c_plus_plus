
#ifndef ABSTRACTBUILDER_HPP
#define ABSTRACTBUILDER_HPP

#include <iostream>
#include <vector>
#include "../template/List.hpp"
#include "../carte/Carte.hpp"
using namespace std;

class AbstractBuilder
{
protected:
    AbstractBuilder(){};
public:
    virtual ~AbstractBuilder() {}
    virtual List<Carte> *build() const = 0;
};

#endif