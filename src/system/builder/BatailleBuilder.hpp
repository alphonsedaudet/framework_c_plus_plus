#ifndef BATAILLEBUILDER_HPP
#define BATAILLEBUILDER_HPP

#include "../carte/Carte.hpp"
#include "../fabrique/FactoryCarte.hpp"
#include "../enum/ConstEnum.hpp"
#include "../template/List.hpp"
#include "AbstractBuilder.hpp"

using namespace std;

/*
* BUilder de la pioche de la bataille 
*/

class BatailleBuilder : public AbstractBuilder
{
private:
    const TaillePioche T;

public:
    BatailleBuilder() = delete;
    BatailleBuilder(const BatailleBuilder &cpy) = delete;
    virtual ~BatailleBuilder();
    BatailleBuilder(const TaillePioche t);
    virtual List<Carte> *build() const;
};

#endif