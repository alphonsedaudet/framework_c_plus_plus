#include "UnoBuilder.hpp"

UnoBuilder::UnoBuilder(TaillePioche t) : taille(t) {}
UnoBuilder::~UnoBuilder() {}

List<Carte> *UnoBuilder ::build() const
{

    List<Carte> *paquet = new List<Carte>();

    if (taille != TaillePioche::CentEtHuit)
        return paquet;

    for (int v = Zero; v <= Neuf; v++){
         for (int c = Bleu; c <= Vert; c++){
             switch (v)
                {
                case Zero:
                    paquet->add(FactoryCarte::buildValueeCarte(v, static_cast<Valeur>(v), static_cast<Couleur>(c), true));
                    break;
                default:
                {
                    paquet->add(FactoryCarte::buildValueeCarte(v, static_cast<Valeur>(v), static_cast<Couleur>(c), true));
                    paquet->add(FactoryCarte::buildValueeCarte(v, static_cast<Valeur>(v), static_cast<Couleur>(c), true));
                }
                break;
                }
         }
    }
    for (int a = PlusDeux; a <= plusQuatre; a++)
    {
        switch (a)
        {
        case PlusDeux:
        {
            for (int c = Bleu; c <= Vert; c++)
            {
                paquet->add(FactoryCarte::buildActionCarte(20, Action::PlusDeux, static_cast<Couleur>(c), true));
                paquet->add(FactoryCarte::buildActionCarte(20, Action::PlusDeux, static_cast<Couleur>(c), true));
            }
        }
        break;

        case InversementDeSens:
        {
            for (int c = Bleu; c <= Vert; c++)
            {
                paquet->add(FactoryCarte::buildActionCarte(20, Action::InversementDeSens, static_cast<Couleur>(c), true));
                paquet->add(FactoryCarte::buildActionCarte(20, Action::InversementDeSens, static_cast<Couleur>(c), true));
            }
        }
        break;

        case PasseTonTour:
        {
            for (int c = Bleu; c <= Vert; c++)
            {
                paquet->add(FactoryCarte::buildActionCarte(20, Action::PasseTonTour, static_cast<Couleur>(c), true));
                paquet->add(FactoryCarte::buildActionCarte(20, Action::PasseTonTour, static_cast<Couleur>(c), true));
            }
        }
        break;

        case Joker:
        {
            for (int i{0}; i < 4; i++)
            {
                paquet->add(FactoryCarte::buildActionCarte(50, Action::Joker, Couleur::None, true));
            }
        }
        break;

        case plusQuatre:
        {
            for (int i{0}; i < 4; i++)
            {
                paquet->add(FactoryCarte::buildActionCarte(50, Action::plusQuatre, Couleur::None, true));
            }
        }
        break;
        default:
            break;
        }
    }
    return paquet;
}