#ifndef TAROTBUILDER_HPP
#define TAROTBUILDER_HPP

#include <iostream>
#include "AbstractBuilder.hpp"
#include "../template/List.hpp"
#include "../enum/ConstEnum.hpp"
#include "../fabrique/FactoryCarte.hpp"

using namespace std;

/*
* BUilder de la pioche du tarot
*/

class TarotBuilder : public AbstractBuilder
{
public:
    TarotBuilder();
    virtual ~TarotBuilder();
    virtual List<Carte> *build() const;
};

#endif