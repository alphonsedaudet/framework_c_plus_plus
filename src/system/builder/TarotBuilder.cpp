#include "TarotBuilder.hpp"

TarotBuilder::TarotBuilder() {}
TarotBuilder::~TarotBuilder() {}

List<Carte> *TarotBuilder::build() const
{
    List<Carte> *paquet = new List<Carte>();
    for (int e = Pique; e <= Carreau; e++)
    {
        for (int v = Deux; v <= Dix; v++)
            paquet->add(
                FactoryCarte::buildSimpleCarte(
                    tarotPoints(true, v),
                    static_cast<Enseigne>(e),
                    static_cast<Valeur>(v),
                    Couleur::None, true));
        for (int v = Valet; v <= As; v++)
            paquet->add(
                FactoryCarte::buildSimpleCarte(
                    tarotPoints(true, v),
                    static_cast<Enseigne>(e),
                    static_cast<Valeur>(v),
                    Couleur::None, true));
    }
    for (int v = Un; v <= VingtEtUn; v++)
        paquet->add(
            FactoryCarte::buildValueeCarte(
                tarotPoints(true, v),
                static_cast<Valeur>(v),
                Couleur::None, true));
    //paquet->add(FactoryCarte::buildJokerCarte(10, Couleur::None, true));

    return paquet;
}

int tarotPoints(bool atout, int v)
{
    if (atout)
    {
        switch (v)
        {
        case Un:
        case VingtEtUn:
        case Roi:
            return 10; // Oudlers et roi
        case Dame:
            return 8;
        case Cavalier:
            return 6;
        case Valet:
            return 4;
        default:
            return 1;
            /* Simplification : un point par paire de carte équivaut à un demi
        * point par carte. Multiplication par 2 pour avoir des entiers.
        */
        }
    }
    else
    {
        return 1;
    }
}