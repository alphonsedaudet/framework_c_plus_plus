#ifndef UNOBUILDER_HPP
#define UNOBUILDER_HPP

#include "../carte/Carte.hpp"
#include "../fabrique/FactoryCarte.hpp"
#include "../enum/ConstEnum.hpp"
#include "../template/List.hpp"
#include "AbstractBuilder.hpp"

using namespace std;

class UnoBuilder : public AbstractBuilder
{
    const TaillePioche taille;

public:
    UnoBuilder(const TaillePioche taille);
    virtual ~UnoBuilder();
    virtual List<Carte> *build() const;
};

#endif
