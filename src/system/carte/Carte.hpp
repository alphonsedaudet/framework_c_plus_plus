#ifndef CARTE_HPP
#define CARTE_HPP

#include <iostream>
#include <string>
#include "../enum/ConstEnum.hpp"

using namespace std;

class Carte
{

private:
  Carte() = delete;
  Carte(const Carte &cpy) = delete;

protected:
  const int point;
  const Couleur couleur;
  bool deFace;
  Carte(int _point, Couleur _couleur = Couleur::None, bool _deFace = false) : point(_point), couleur(_couleur), deFace(_deFace){/* cout << "create carte" << endl;*/};

public:
  virtual ~Carte(){/*cout << "destruction carte" << endl;*/};
  virtual bool estDeFace() { return deFace; };
  virtual void retourner() { deFace = !deFace; };
  virtual int getPoint() const { return this->point; };
  virtual int getCouleur() const { return this->couleur; };
  virtual bool operator==(const Carte &c) const { return this->hash().compare(c.hash()) == 0; }
  friend ostream &operator<<(ostream &os, const Carte &c) { return os << c.print(); }
  virtual string hash() const { return to_string(point) + to_string(couleur) + to_string(deFace); };
  virtual string print() const { return " Point : " + to_string(point) + " Couleur : " + to_string(couleur); };
  virtual TypeCarte getType() const = 0;
};

#endif