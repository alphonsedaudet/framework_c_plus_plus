#include "Valuee.hpp"

Valuee ::Valuee(int p, Valeur v, Couleur c, bool f) : Carte(p, c, f), valeur(v) {}

Valuee ::~Valuee() {}

Valeur Valuee ::getValeur() const
{
  return this->valeur;
}

bool Valuee::operator==(const Valuee &c)
{
  return (Carte::operator==(c) && this->valeur == c.valeur);
}
string Valuee::print() const
{
  return "Atout " + to_string(valeur); // TODO c'est moche, à revoir
}

string Valuee::hash() const { return to_string(this->getType()) + to_string(point) + to_string(couleur) + to_string(valeur); }
TypeCarte Valuee::getType() const { return TypeCarte::TypeValuee; }