#ifndef CARTESIMPLE_HPP
#define CARTESIMPLE_HPP

#include <iostream>
#include <string>
#include "Carte.hpp"
#include "Valuee.hpp"
#include "../enum/ConstEnum.hpp"

using namespace std;

class CarteSimple : public Valuee
{
  friend class FactoryCarte;

private:
  CarteSimple() = delete;
  CarteSimple(int point, Enseigne e, Valeur v, Couleur c = Couleur::None, bool deFace = false);
  virtual ~CarteSimple();

protected:
  const Enseigne enseigne;

public:
  Enseigne getEnseigne() const;
  virtual bool operator==(const CarteSimple &c);
  virtual string print() const;
  virtual string hash() const;
  virtual TypeCarte getType() const;
};

#endif
