#include "CarteSimple.hpp"

CarteSimple::CarteSimple(int p, Enseigne e, Valeur v, Couleur c, bool deFace) : Valuee(p, v, c, deFace), enseigne(e) {}

CarteSimple::~CarteSimple() {}

TypeCarte CarteSimple::getType() const { return TypeCarte::TypeSimple; }

string CarteSimple::hash() const
{
    return to_string(this->getType()) + to_string(point) + to_string(couleur) +
           to_string(enseigne) + to_string(valeur);
}

string CarteSimple::print() const
{
    string r = "";
    switch (this->enseigne)
    {
    case Pique:
        r += "Pique";
        break;
    case Coeur:
        r += "Cœur";
        break;
    case Trefle:
        r += "Trèfle";
        break;
    case Carreau:
        r += "Carreau";
    }
    r += " ";
    switch (this->valeur)
    {
    case Un:
        r += "1";
        break;
    case Deux:
        r += "2";
        break;
    case Trois:
        r += "3";
        break;
    case Quatre:
        r += "4";
        break;
    case Cinq:
        r += "5";
        break;
    case Six:
        r += "6";
        break;
    case Sept:
        r += "7";
        break;
    case Huit:
        r += "8";
        break;
    case Neuf:
        r += "9";
        break;
    case Dix:
        r += "10";
        break;
    case Valet:
        r += "Valet";
        break;
    case Dame:
        r += "Dame";
        break;
    case Roi:
        r += "Roi";
        break;
    default:
        break;
    }
    return r + Carte::print();
}

bool CarteSimple::operator==(const CarteSimple &c)
{
    return (Valuee::operator==(c) && this->enseigne == c.enseigne);
}

Enseigne CarteSimple::getEnseigne() const
{
    return this->enseigne;
}
