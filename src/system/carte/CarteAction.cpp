#include "CarteAction.hpp"

CarteAction::CarteAction(int p, Couleur c, Action action, bool deFace) : Carte(p, c, deFace), action(action) {}

CarteAction::~CarteAction() {}

string CarteAction::hash() const { return Carte::hash() + to_string(action); }

string CarteAction::print() const { return " Action : " + to_string(action) + Carte::print(); }

TypeCarte CarteAction::getType() const { return TypeCarte::TypeAction; }

Action CarteAction ::getAction() const { return action; }
