#ifndef VALUEE_HPP
#define VALUEE_HPP

#include "Carte.hpp"
#include "../enum/ConstEnum.hpp"

class Valuee : public Carte
{
  friend class FactoryCarte;

private:
  Valuee() = delete;
  Valuee(const Valuee &cpy) = delete;

protected:
  const Valeur valeur;

  Valuee(int p, Valeur v, Couleur c = Couleur::None, bool deFace = false);
  virtual ~Valuee();

public:
  virtual Valeur getValeur() const;
  virtual bool operator==(const Valuee &c);
  virtual string print() const;
  virtual string hash() const;
  virtual TypeCarte getType() const;
};

#endif
