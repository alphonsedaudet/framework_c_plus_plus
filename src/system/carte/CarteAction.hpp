#ifndef CARTEACTION_HPP
#define CARTEACTION_HPP

#include <string>
#include "../carte/Carte.hpp"
#include "../enum/ConstEnum.hpp"

using namespace std;

class CarteAction : public Carte
{
private:
    const Action action;
    CarteAction() = delete;
    CarteAction(const CarteAction &cpy) = delete;

public:
    CarteAction(int p, Couleur c, Action action, bool deFace = false);
    virtual ~CarteAction();
    virtual string hash() const;
    virtual string print() const;
    virtual TypeCarte getType() const;
    Action getAction() const;
};

#endif