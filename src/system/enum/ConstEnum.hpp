#ifndef CONSTENUM_HPP
#define CONSTENUM_HPP

/*
* Enseigne pour les cartes simples
*/
enum Enseigne
{
  Pique = 1,
  Coeur = 2,
  Trefle = 3,
  Carreau = 4
};

enum TypeCarte
{
  TypeSimple = 1,
  TypeValuee,
  TypeAction
};

enum Action
{
  PlusDeux = 1,
  InversementDeSens,
  PasseTonTour,
  Joker,
  plusQuatre
};

/*
* Valeur des cartes et points
*/
enum Valeur
{
  Zero = 0,
  Un = 1,
  Deux = 2,
  Trois = 3,
  Quatre = 4,
  Cinq = 5,
  Six = 6,
  Sept = 7,
  Huit = 8,
  Neuf = 9,
  Dix = 10,
  Onze = 11,
  Douze = 12,
  Treize = 13,
  Quatorze = 14,
  Quinze = 15,
  Seize = 16,
  DixSept = 17,
  DixHuit = 18,
  DixNeuf = 19,
  Vingt = 20,
  VingtEtUn = 21,
  Cavalier = 22,
  Valet = 23,
  Dame = 24,
  Roi = 25,
  As = 26
};

/*
* Taille de la Pioche 
*/
enum TaillePioche
{
  TrenteDeux = 32,
  CinquanteDeux = 52,
  CentEtHuit = 108
};
// ajouter 54 avec deux Jokers?

/*
* Couleur des cartes pour le jeux de Uno
*/
enum Couleur
{
  Bleu = 1,
  Rouge = 2,
  Jaune = 3,
  Vert = 4,
  None
};

#endif