# LOA projet framework

Framework en C++11 pour jeux de cartes.

## Comment implémenter un jeu de carte ?

Pour implémenter un jeu il faut créer une classe `MonJeu` dans
`src/jeux/monJeu/` héritant de `Jeu`, et implémenter ses méthodes virtuelles
`init`, `lancer` et `score`. `init` doit initialiser les attributs du jeu
(joueurs, plateau et paquet). `lancer` lance le jeu proprement dit, et une fois
finit `score` doit afficher les résultats (s'il y en a, sinon ne fait rien).

## Architecture

Le _framework_ est dans `src/system/`. Il propose différents types de cartes,
des `Builder` permettant de créer les paquets de carte, des classes `Plateau`,
`Joueur` et `List` permettant de gérer les composants génériques nécessaires aux
jeux de carte.
